const fs = require("fs");
const express = require("express");
const child_process = require("child_process");
const { join } = require("path");
const serverapt = require("./serverapt");

const TYPE = "text";
const MODEL = "open_llama_3b";
const DATA = {};

let models = [];

let app = express();

let workers = [];

const API_KEY = "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";

let defaultPrompts = {
  prompts: [],
}

console.log(__dirname);

let queue = {
  prompts: [],
  promptPtr: 0,
  getFirst: () => {
    for(let i = 0; i < queue.prompts.length; i++)
    {
      if(!queue.prompts[i].started)
      {
        return queue.prompts[i];
      }
    }
    return null;
  }
};

const queue_get = (id) => {
  for(let i = 0; i < queue.prompts.length; i++)
  {
    if(queue.prompts[i].id == id)
    {
      return queue.prompts[i];
    }
  }
};

const queue_set = (id, val) => {
  for(let i = 0; i < queue.prompts.length; i++)
  {
    if(queue.prompts[i].id == id)
    {
      queue.prompts[i] = val;
    }
  }
};

const queue_remove = (id) => {
  for(let i = 0; i < queue.prompts.length; i++)
  {
    if(queue.prompts[i].id == id)
    {
      queue.prompts.splice(i, 1);
    }
  }
}

const ensure_prompt_dir = () => {
  if(!fs.existsSync("prompts"))
  {
    fs.mkdirSync("prompts");
  }
  if(!fs.existsSync("prompts/list.json"))
  {
    fs.writeFileSync("prompts/list.json", JSON.stringify({
      prompts: [],
    }));
  }
  if(!fs.existsSync("public/images"))
  {
    fs.mkdirSync("public/images");
  }
}

const promptcat = (id, response) => {
  let lastPrompt = null;
  ensure_prompt_dir();
  if(!id)
  {
    console.log("WARN: Attempt to access prompt with no ID!");
    return;
  }

  if(fs.existsSync("prompts/prompt_" + id + ".json"))
  {
    lastPrompt = JSON.parse(fs.readFileSync("prompts/prompt_" + id + ".json", "utf8"));
  }
  if(lastPrompt)
  {
    if(!lastPrompt.prompt)
    {
      lastPrompt.prompt = queue_get(id).prompt ? queue_get(id).prompt : "";
    }
    lastPrompt.response += response;
  }
  if(queue_get(id) && queue_get(id).response)
  {
    queue_set(id, {
      ...queue_get(id),
      response: queue_get(id).response + response,
      prompt: lastPrompt ? lastPrompt.prompt : queue_get(id).prompt,
    });
  }
  else if(queue_get(id))
  {
    const qp = queue_get(id);
    qp.response = response;
    queue_set(id, qp);
  }
  fs.writeFileSync("prompts/prompt_" + id + ".json", JSON.stringify({
    id, prompt: lastPrompt ? lastPrompt.prompt : queue_get(id).prompt, response: lastPrompt ? lastPrompt.response : response,
  }));
  let prompts = JSON.parse(fs.readFileSync("prompts/list.json", "utf8"));
  let found = false;
  prompts.prompts.forEach((promt) => {
    if(promt.id == id)
    {
      found = true;
    }
  });
  if(!found)
  {
    prompts.prompts.push({
      id,
      path: join(__dirname, "prompts", "prompt_" + id + ".json"),
    });
    fs.unlink("prompts/list.json", () => {
      fs.writeFileSync("prompts/list.json", JSON.stringify(prompts));
    });
  }
}

const homepage = (req, res) => {
  return res.status(200).format({
    'text/html': () => { res.send(fs.readFileSync("public/index.html")); },
  });
}

app.get('/', homepage);

app.use(express.static("public"))

app.get('/workers/status', (req, res) => {
  return res.json(workers);
});

app.get('/workers/join/:name/:models', (req, res) => {
  const name = req.params.name;
  let mdls = req.params.models;
  if(!name)
  {
    return res.json(401, {
      "status": "error",
    });
  }
  let spammer = false;
  if(!mdls)
  {
    if(!models)
    {
      return res.status(400, {status: "error"});
    }
    console.log("Warning: worker '" + name + "' connected with no models. Spammer?");
  }
  workers.push(name);
  res.status(200).json({
    status: "ok",
  });
})

app.get('/queue/prompt/list', (req, res) => {
  return res.status(200).json(queue.prompts);
});

app.get('/queue/prompt/new/:prompt/:type/:model/:data', (req, res) => {
  const prompt = req.params.prompt;
  const type = req.params.type;
  const model = req.params.model;
  const data = req.params.data;
  if(!prompt || !type || !model)
  {
    res.status(401).json({
      "status": "error",
    });
  }
  if(!data) {data = {};}
  const prompts = JSON.parse(fs.readFileSync("prompts/list.json", "utf8"));
  queue.prompts.push({
    prompt: prompt,
    text: prompt,
    response: "",
    finished: false,
    started: false,
    id: prompts.prompts.length,
    type, model, data,
    outfile: ((prompts.length) + ".png"),
    progress: {start: 0, current: 0},
  });
  promptcat(prompts.prompts.length, "");
  return res.status(200).json({
    status: "ok",
  });
});

app.get('/queue/prompt/next', (req, res) => {
  const prompt = queue.getFirst();
  if(prompt != null)
  {
    res.status(200).json({
      status: "ok",
      response: {
        prompt: prompt.text,
        id: prompt.id,
        model: prompt.model,
        type: prompt.type,
        data: prompt.data,
      }
    });
    queue_set(prompt.id, {
      text: prompt.text,
      response: "",
      finished: false,
      started: true,
      id: prompt.id,
      model: prompt.model,
      type: prompt.type,
      data: prompt.data,
    });
    return;
  }
  return res.status(200).json({
    status: "empty",
  });
});

app.get('/queue/jobs/:id/finish', (req, res) => {
  const id = req.params.id;
  queue_remove(id);
  return res.status(200).json({
    status: "ok",
  });
});

app.get('/queue/jobs/:id/output/:text', (req, res) => {
  ensure_prompt_dir();
  const id = req.params.id;
  const text = req.params.text;
  const prompt = queue_get(id);
  console.log(text);
  if(!prompt)
  {
    return res.status(400).json({
      status: "error",
    });
  }
  let json;
  try {
    json = JSON.parse(text);
  } catch(e) { /*console.error(e);*/ }
  if(json)
  {
    if(json.progress)
    {
      prompt.current = json.current;
      queue_set(id, prompt);
      return res.status(200).json({
        status: "ok",
      })
    }
    if(json.message)
    {
      let aiws;
      try {
        aiws = JSON.parse(json.message);
      } catch(e) { /*console.error(e);*/ }
      if(aiws && aiws.aiworker && aiws.aiworker.action)
      {
        if(aiws.aiworker.action == "exec")
        {
          queue_remove(id);
          return res.status(200).json({
            status: "ok",
          });
        }
      }
      promptcat(id, json.message);
      return res.status(200).json({
        status: "ok",
      });
    }
  }
  const result = {
    prompt: prompt.text,
    response: text,
  };
  promptcat(id, result.response);
  return res.status(200).json({
    status: "ok",
  });
});

app.get('/results/prompts/list', (req, res) => {
  ensure_prompt_dir();
  //const promptList = JSON.stringify(fs.readFileSync("prompts/list.json"));
  //console.log(promptList);
  let fl = fs.readFileSync("prompts/list.json", "utf8");
  const promptList = JSON.parse(fl);
  const prompts = [];
  for(let i = 0; i < promptList.prompts.length; i++)
  {
    //console.log("Found prompt: " + fs.readFileSync(promptList.prompts[i].path, "utf8"));
    const prompt = JSON.parse(fs.readFileSync(promptList.prompts[i].path, "utf8"));
    prompts.push({
      id: promptList.prompts[i].id,
      prompt: prompt.prompt,
      response: prompt.response,
    });
  }
  return res.status(200).json({
    status: "ok",
    prompts,
  })
});

app.get('/queue/image/new/:base64', async (req, res) => {
  const base64 = req.params.base64;
  if(!base64)
  {
    return res.status(400).json({
      status: "error",
    });
  }
  const buffer = Buffer.from(base64, "base64");
  let fileIndex = 1;
  fs.readdirSync("public/images").forEach((file) => {
    fileIndex++;
  });
  writeFileSync('public/images/' + fileIndex + '.png', buffer);
});

app.use(require('./serverapt.js'));

app.get('/image/get/:id', (req, res) => {
  const id = req.params.prompt;
  if(!id)
  {
    return res.status(400).json({
      status: "error",
    });
  }
  let base64 = "";
  if(fs.existsSync("public/images/img_" + id + ".png"))
  {
    base64 = fs.readFileSync("public/images/img_" + id + ".png", "base64");
  }
  return res.status(200).json({
    status: "ok",
    base64,
  });
});

app.get('/image/add/:id/:base64', (req, res) => {
  const id = req.params.id;
  const base64 = req.params.base64;
  if(!id || !base64)
  {
    return res.status(400).json({
      status: "error",
    });
  }
  const buffer = Buffer.from(base64, "base64");
  fs.writeFileSync("public/images/img_" + id + ".png", buffer);
  return res.status(200).json({
    status: "ok",
  });
});

// Sets up a listener to look through queue and execute prompts
// using a fetch call to http://localhost:8000/v1/chat/completions
/* Format:
  {
    "model": "",
    input: prompt,
  }
*/
const queueListener = () => {
  const prompt = queue.getFirst();
  if(prompt != null)
  {
    if(!prompt.started)
    {
      prompt.started = true;
      queue_set(prompt.id, prompt);
    }
    const model = prompt.model;
    const type = prompt.type;
    const data = prompt.data;
    const input = prompt.prompt;
    const id = prompt.id;
    const outfile = prompt.outfile;
    const progress = prompt.progress;
    const fetch = require("node-fetch");
    const body = {
      "model": model,
      "input": input,
      "type": type,
      "data": data,
      "id": id,
      "outfile": outfile,
      "progress": progress,
    };
    fetch("http://localhost:8000/v1/chat/completions", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": API_KEY,
      },
      body: JSON.stringify(body),
    }).then((res) => {
      return res.json();
    }).then((json) => {
      if(json && json.id)
      {
        queue_set(json.id, {
          ...queue_get(json.id),
          progress: json.progress,
          ...json,
        });
      }
    });
  }
}

const server = app.listen(8083, () => {
  if(!queue_get(0)) { threadId.sleep(10);} 
  console.log("Listening on port 8083!");
});
server.on('connection', (object) => {
  console.log("New connection!");
  // 1 Hour timeout. WHOO!
  object.setTimeout(((60 * 60) * 60) * 1000);
  threadId.sleep(10);
});