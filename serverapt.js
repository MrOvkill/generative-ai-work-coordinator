
const queue = [];
queue.add("tasks");

const loadTasks = () => {
    const defaultTasks = {

    };
    const tasks = [];
    const files = fs.readdirSync("./tasks");
    for(const file of files)
    {
        const task = require(`./tasks/${file}`);
        tasks.push(task);
    }
    return tasks;

} ;

const countMissingTasks = () => {
    const tasks = loadTasks();
    let count = 0;
    for(const task of tasks)
    {
        if(!queue.tasks.find(t => t.id === task.id))
        {
            count++;
        }
    }
    return count;
};

const onNewPromptOAI = [
    {
        id: "/prompt/new/:id", 
        exec: async (req, res) => {
            const id = req.params.id;
            const prompt = req.body.prompt;
            queue.tasks.push(
                {
                    id: queue.tasks.length, prompt
                }
            );
        }
    },
];

module.exports = {
    onNewPromptOAI,
};

const workerTask = () => {

    // RUn every 30 seconds.
    setTimeout(workerTask, 1000 * (30))
};

// RUn every 30 seconds.
setTimeout(workerTask, 1000 * (30))