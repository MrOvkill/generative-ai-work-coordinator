let keepRunning = true;

let queue = [];

document.getElementById("prompt").value = `
## HUMAN:
\${query}

## RESPONSE:

`;

async function submitPromptToWorkers() {
  const query = document.getElementById("query").value;
  document.getElementById("query").value = "";
  const prompt = document.getElementById("prompt").value.replace("${query}", query);
  fetch("/queue/prompt/new/" + encodeURIComponent(prompt) + "/text/open_llama_7b_slow/" + encodeURIComponent(JSON.stringify({}))).then((data) => {
    if(!data)
    {
      console.log("Server error!");
    }
    return data.json();
  }).then((data) => {
    if(data.status == "ok")
    {
      registerPromptUpdater();
      console.log("Prompt queued.");
      return;
    }
    console.log("Prompt queue error.");
  });
}

async function submitSDPromptToWorkers() {
  const prompt = document.getElementById("sd_prompt").value;
  const data = document.getElementById("data").value;
  fetch('/queue/prompt/new/' + encodeURIComponent(prompt) + '/image/stable_diffusion_mm2_1/' + encodeURIComponent(JSON.stringify(data))).then((data) => {
    if(!data){
      return null;
    }
    return data.json();
  }).then((data) => {
    if(data)
    {
      console.log("Queued Image!");
      registerPromptUpdater();
    }
  })
}

async function renderTable(prompt)
{
  let html = "<html><thead><tr><td>";
  for(let c in prompt[0])
  {
    html+="<td>" + c + "</td>";
  }
  html += "</thead><tbody>"; for(let d of prompt) 
  { 
    html += "<tr>"; 
    for(let key in d) 
    { 
      html += "<td>" + d[key] + "</td>"; 
    } 
    html += "</tr>"; 
  }
  return html;
}

async function renderPromptList(prompts)
{
  let html = "";
  for(let i = 0; i < prompts.length; i++)
  {
    console.log(prompts[i]);
    html += renderTable(prompts[i]);
  }
  return html;
}

async function readResponse(res)
{
  let stream = res.getReader();
  let data = stream.read();
  let dat = "";
  while(data)
  {
    dat += data;
    data = res.read();
  }
  return dat;
}

const createTable = (data) =>
{
  console.log(data);
  let html = "";
  for(let i = 0; i < data.length; i++)
  {
    html += "<tr><td>" + data[i].text + "</td><td>" + data[i].response + "</td>";
  }
  html += "";
  return html;
}

const createResultsTable = (data) =>
{
  let html = "<table class=\"table\"><thead><tr><td width=\"25%\">Prompt</td><td width=\"75%\">Response</td></tr></thead><tbody>";
  for(let i = data.length-1; i > -1; i--)
  {
    html += "<tr><td>" + data[i].prompt + "</td><td>" + data[i].response + "</td>";
    if(data[i].progress)
    {
      html += `<td>${data[i].progress.current}</td>`;
    }
  }
  html += "</tbody></table>";
  return html;
}

const renderImageTable = (images) => {

};

async function registerPromptUpdater()
{
  if(!keepRunning) { return }
  window.setTimeout(() => {
    fetch('/queue/prompt/list').then((data) => {
      return data.json();
    }).then((data) => {
      console.log(JSON.stringify(data));
      document.getElementById("promptqueue").innerHTML = createTable(data);
    });
    fetch('/results/prompts/list').then((data) => {
      return data.json();
    }).then((data) => {
      console.log(data);
      document.getElementById("promptresults").innerHTML = createResultsTable(data.prompts);
    });
    registerPromptUpdater();
  }, 1500);
}

registerPromptUpdater();